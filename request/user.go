package request

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/satori/go.uuid"

	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/ctx"
	"gitlab.com/mydocge/shared/model"
	"gitlab.com/mydocge/shared/util"
)

// User gets token from request and returns the user
func User(req *http.Request) (*model.User, error) {
	token, err := util.Token(req)
	if err != nil {
		return nil, util.LogErr(err) //NatsResponse{Result: ResultTypeUnspecifiedError, Error: err.Error()}
	}
	// lang, err := util.Language(req)
	// if err != nil {
	// 	return nil, err //NatsResponse{Result: ResultTypeBadLanguage, Error: err.Error()}
	// }
	return UserByToken(token)
}

// UserByToken requests user from user micro-service and returns it
func UserByToken(token string) (*model.User, error) {
	uir := model.UserInfoRequest{Token: token}
	uirBytes, err := json.Marshal(uir)
	if err != nil {
		return nil, util.LogErr(err)
	}

	auRes, err := ctx.Nats().Request(constant.EventTypeAuthInfo.String(), uirBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(auRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(auRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.User, nil
}

// UserByID returns user by specified id
func UserByID(uir model.UserInfoRequest) (*model.User, error) {
	uirBytes, err := json.Marshal(uir)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeUserInfoByID.String(), uirBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.User, nil
}

// DeleteUserByID sets deleted at to now for user with specified id
func DeleteUserByID(uir model.UserInfoRequest) (*model.User, error) {
	uirBytes, err := json.Marshal(uir)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeDeleteUserByID.String(), uirBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.User, nil
}

// UsersByIdentifier returns users with specified personal identifier
func UsersByIdentifier(uir model.UserInfoRequest) (*[]model.UserPubInfo, error) {
	uirBytes, err := json.Marshal(uir)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeUsersByIdentifier.String(), uirBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return &res.UserPubInfos, nil
}

// UserByEmail returns user with specified email
func UserByEmail(uir model.UserInfoRequest) (*model.User, error) {
	uirBytes, err := json.Marshal(uir)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeUserInfoByEmail.String(), uirBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no data"))
	}
	var res NatsResponse
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.User, nil
}

// UserSocketsByUserIDs returns user with specified email
func UserSocketsByUserIDs(userIDs map[uuid.UUID]bool) (map[uuid.UUID][]string, error) {
	uIDs := []uuid.UUID{}
	for uID := range userIDs {
		uIDs = append(uIDs, uID)
	}
	uIDsBytes, err := json.Marshal(uIDs)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeUserSocketInfos.String(), uIDsBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no data"))
	}
	var res NatsResponse
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.UserSockets, nil
}

// UserSocketsByUserID returns user's socket connection ids
func UserSocketsByUserID(userID uuid.UUID) ([]string, error) {
	uIDBytes, err := json.Marshal(userID)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeUserSocketInfoByUserID.String(), uIDBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no data"))
	}
	var res NatsResponse
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	if val, ok := res.UserSockets[userID]; ok {
		return val, nil
	}
	return []string{}, nil
}

// CreateUser handle create user request via NATS
func CreateUser(user model.User) (*model.User, error) {
	userBytes, err := json.Marshal(user)
	if err != nil {
		return nil, util.LogErr(err)
	}
	userCreateRes, err := ctx.Nats().Request(constant.EventTypeCreateUserRequest.String(), userBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(userCreateRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no data"))
	}
	err = json.Unmarshal(userCreateRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.User, nil
}

// UserPubInfoByIDs returns a list of user pub infos
func UserPubInfoByIDs(idsMap map[uuid.UUID]bool) (*map[uuid.UUID]model.UserPubInfo, error) {
	if len(idsMap) == 0 {
		return nil, nil
	}
	uIDs := []uuid.UUID{}
	for val := range idsMap {
		uIDs = append(uIDs, val)
	}
	uIDsBytes, err := json.Marshal(uIDs)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeUserPubInfoByIDs.String(), uIDsBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	usersMap := make(map[uuid.UUID]model.UserPubInfo)
	for _, user := range res.UserPubInfos {
		usersMap[user.ID] = user
	}
	return &usersMap, nil
}

// UserFullInfoByIDs returns a list of user pub infos
func UserFullInfoByIDs(idsMap map[uuid.UUID]bool) (*map[uuid.UUID]model.User, error) {
	if len(idsMap) == 0 {
		return nil, nil
	}
	uIDs := []uuid.UUID{}
	for val := range idsMap {
		uIDs = append(uIDs, val)
	}
	uIDsBytes, err := json.Marshal(uIDs)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeUserFullInfoByIDs.String(), uIDsBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	usersMap := make(map[uuid.UUID]model.User)
	for _, user := range res.UserFullInfos {
		usersMap[user.ID] = user
	}
	return &usersMap, nil
}

// ClinicPubInfoByIDs returns a list of user pub infos
func ClinicPubInfoByIDs(idsMap map[uuid.UUID]bool) (*map[uuid.UUID]model.UserPubInfo, error) {
	if len(idsMap) == 0 {
		return nil, nil
	}
	cIDs := []uuid.UUID{}
	for val := range idsMap {
		cIDs = append(cIDs, val)
	}
	cIDsBytes, err := json.Marshal(cIDs)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeClinicPubInfoByIDs.String(), cIDsBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	usersMap := make(map[uuid.UUID]model.UserPubInfo)
	for _, user := range res.UserPubInfos {
		usersMap[user.ID] = user
	}
	return &usersMap, nil
}

// OperatorsByClinicID returns a list of operator user pub infos by clinic id
func OperatorsByClinicID(clinicID uuid.UUID) (*[]model.UserPubInfo, error) {
	uir := model.UserInfoRequest{ClinicID: clinicID}
	uirBytes, err := json.Marshal(uir)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeOperatorsByClinicID.String(), uirBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return &res.UserPubInfos, nil
}
