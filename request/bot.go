package request

import (
	"encoding/json"
	"errors"

	"github.com/satori/go.uuid"

	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/ctx"
	"gitlab.com/mydocge/shared/model"
	"gitlab.com/mydocge/shared/util"
)

// BotByID returns user by specified id
func BotByID(bID uuid.UUID) (*model.Bot, error) {
	bIDBytes, err := json.Marshal(bID)
	if err != nil {
		return nil, util.LogErr(err)
	}
	bRes, err := ctx.Nats().Request(constant.EventTypeBotInfoByID.String(), bIDBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(bRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(bRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.Bot, nil
}
