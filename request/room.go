package request

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/ctx"
	"gitlab.com/mydocge/shared/model"
	"gitlab.com/mydocge/shared/util"
)

// UserInterests returns user ids which user is intereseted in
func UserInterests(user model.User) (*[]uuid.UUID, error) {
	userBytes, err := json.Marshal(user)
	res, err := ctx.Nats().Request(constant.EventTypeUserInterests.String(), userBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var natsRes NatsResponse
	if len(res.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(res.Data, &natsRes)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if natsRes.Error != nil {
		return nil, util.LogErr(natsRes.Err())
	}
	return &natsRes.UserInterests, nil
}

// RoomOtherUsers returns user ids which user is intereseted in
func RoomOtherUsers(uID, rID uuid.UUID) (*map[uuid.UUID]bool, error) {
	user, err := UserByID(model.UserInfoRequest{ID: uID})
	if err != nil {
		return nil, util.LogErr(err)
	}
	uir := model.UserInfoRequest{ID: user.ID, Type: user.Type, RoomID: rID}
	uirBytes, err := json.Marshal(uir)
	res, err := ctx.Nats().Request(constant.EventTypeRoomOtherUsers.String(), uirBytes, 300*time.Millisecond)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var natsRes NatsResponse
	if len(res.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(res.Data, &natsRes)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if natsRes.Error != nil {
		return nil, util.LogErr(natsRes.Err())
	}
	otherUsers := make(map[uuid.UUID]bool)
	for _, uID := range natsRes.RoomOtherUsers {
		otherUsers[uID] = true
	}
	return &otherUsers, nil
}
