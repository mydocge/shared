package request

import (
	"encoding/json"
	"errors"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/ctx"
	"gitlab.com/mydocge/shared/model"
	"gitlab.com/mydocge/shared/util"
)

// ClinicServicesByID returns a list of user pub infos
func ClinicServicesByID(clinicID uuid.UUID) (*model.ClinicServices, error) {
	cIDBytes, err := json.Marshal(clinicID)
	if err != nil {
		return nil, util.LogErr(err)
	}
	uRes, err := ctx.Nats().Request(constant.EventTypeClinicServicesByID.String(), cIDBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(uRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(uRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return &res.ClinicServices, nil
}
