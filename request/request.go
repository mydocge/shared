package request

import (
	"errors"
	"net/http"

	"github.com/satori/go.uuid"

	"gitlab.com/mydocge/shared/model"
	"gitlab.com/mydocge/shared/util"
)

type ResultType string

type ErrorResponse struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

// NatsResponse describes data exchange response via NATS
type NatsResponse struct {
	Error            *ErrorResponse          `json:"error"`
	User             *model.User             `json:"user"`
	PatientProfile   *model.PatientProfile   `json:"patient_profile"`
	Bot              *model.Bot              `json:"bot"`
	InfoNotification *model.InfoNotification `json:"info_notification"`
	ClinicServices   model.ClinicServices    `json:"clinic_services"`
	UserSockets      map[uuid.UUID][]string  `json:"user_sockets"`
	UserInterests    []uuid.UUID             `json:"user_interests"`
	RoomOtherUsers   []uuid.UUID             `json:"room_other_users"`
	UserPubInfos     []model.UserPubInfo     `json:"user_pub_infos"`
	UserFullInfos    []model.User            `json:"user_full_infos"`
}

func (nr NatsResponse) Err() error {
	if nr.Error == nil {
		return nil
	}
	switch nr.Error.Code {
	case http.StatusBadRequest:
		return util.ErrorBadRequest{Message: nr.Error.Message}
	case http.StatusUnauthorized:
		return util.ErrorStatusUnauthorized{}
	default:
		return errors.New(nr.Error.Message)
	}
}
