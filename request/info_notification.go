package request

import (
	"encoding/json"
	"errors"

	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/ctx"
	"gitlab.com/mydocge/shared/model"
	"gitlab.com/mydocge/shared/util"
)

// CreateNotification handle create notification request via NATS
func CreateNotification(incr model.InfoNotificationCreateRequest) (*model.InfoNotification, error) {
	// TODO: decide whether this method returns notification to service or the creating service itself notifies users correspondingly
	incrBytes, err := json.Marshal(incr)
	if err != nil {
		return nil, util.LogErr(err)
	}
	notificationCreateRes, err := ctx.Nats().Request(constant.EventTypeCreateNotificationRequest.String(), incrBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(notificationCreateRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no data"))
	}
	err = json.Unmarshal(notificationCreateRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.InfoNotification, nil
}
