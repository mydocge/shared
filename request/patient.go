package request

import (
	"encoding/json"
	"errors"

	"github.com/satori/go.uuid"

	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/ctx"
	"gitlab.com/mydocge/shared/model"
	"gitlab.com/mydocge/shared/util"
)

// PatientInfoByID returns user by specified id
func PatientInfoByID(pID uuid.UUID) (*model.PatientProfile, error) {
	pIDBytes, err := json.Marshal(pID)
	if err != nil {
		return nil, util.LogErr(err)
	}
	pRes, err := ctx.Nats().Request(constant.EventTypePatientInfoByID.String(), pIDBytes, constant.NatsTimeout)
	if err != nil {
		return nil, util.LogErr(err)
	}
	var res NatsResponse
	if len(pRes.Data) == 0 {
		return nil, util.LogErr(errors.New("no response data"))
	}
	err = json.Unmarshal(pRes.Data, &res)
	if err != nil {
		return nil, util.LogErr(err)
	}
	if res.Error != nil {
		return nil, util.LogErr(res.Err())
	}
	return res.PatientProfile, nil
}
