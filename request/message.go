package request

import (
	"encoding/json"

	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/ctx"
	"gitlab.com/mydocge/shared/model"
	"gitlab.com/mydocge/shared/util"
)

// PublishMessage handles publish message for pusher
func PublishMessage(msg model.Message) error {
	msgBytes, err := json.Marshal(msg)
	if err != nil {
		return util.LogErr(err)
	}
	return ctx.Nats().Publish(constant.EventTypePush.String(), msgBytes)
}
