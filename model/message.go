package model

import (
	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
)

// Message is struct for pushing messages to user via websocket
type Message struct {
	To   []string    `json:"to"`
	Data MessageInfo `json:"data"`
}

// MessageInfo is Data of message
type MessageInfo struct {
	Type    constant.MessageType `json:"type"`
	Payload interface{}          `json:"payload"`
}

// UserStatusInfo is user online or offline
type UserStatusInfo struct {
	UserID   uuid.UUID `json:"user_id"`
	IsOnline bool      `json:"is_online"`
}

type IsTypingInfo struct {
	IsTyping bool      `json:"is_typing"`
	RoomID   uuid.UUID `json:"room_id"`
	// UserID   uuid.UUID `json:"user_id"`
}

// WebRTCConn struct for exchanging webrtc peer data
type WebRTCConn struct {
	Sdp  string `json:"sdp"`
	Type string `json:"type"`
}

type CallingInfo struct {
	Peer *WebRTCConn `json:"peer"`
	// UserID uuid.UUID   `json:"user_id"`
	// RoomID uuid.UUID   `json:"room_id"`
	PatientID       uuid.UUID `json:"patient_id"`
	UserID          uuid.UUID `json:"user_id"`
	IsPatientAuthor bool      `json:"is_patient_author"`
}
