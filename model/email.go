package model

import (
	"time"

	"github.com/satori/go.uuid"
)

// Email is struct for moving email data between micro-services and frontend
type Email struct {
	ID          uuid.UUID `json:"id"`
	Address     string    `json:"address"`
	IsConfirmed bool      `json:"is_confirmed"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}
