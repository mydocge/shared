package model

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/satori/go.uuid"
	"github.com/torniker/infermedica"
	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/util/logger"
)

type AggregateInteractions struct {
	PatientID    uuid.UUID              `json:"patient_id"`
	UserID       uuid.UUID              `json:"user_id"`
	ClinicID     uuid.UUID              `json:"clinic_id"`
	Interactions []AggregateInteraction `json:"interactions"`
	Sex          *infermedica.Sex       `json:"sex"`
}

type AggregateInteraction struct {
	Author      constant.AuthorType `json:"author"`
	AggregateID *string             `json:"aggregate_id"`
	Handle      string              `json:"handle"`
	Payload     *json.RawMessage    `json:"payload"`
	CreatedAt   time.Time           `json:"created_at"`
}

type Interaction struct {
	PatientID   uuid.UUID           `json:"patient_id"`
	UserID      uuid.UUID           `json:"user_id"`
	ClinicID    uuid.UUID           `json:"clinic_id"`
	Author      constant.AuthorType `json:"author"`
	AggregateID *string             `json:"aggregate_id"`
	Handle      string              `json:"handle"`
	Payload     *json.RawMessage    `json:"payload"`
	CreatedAt   time.Time           `json:"created_at"`
}

func (i *Interaction) Aggregate() AggregateInteraction {
	return AggregateInteraction{
		Author:      i.Author,
		AggregateID: i.AggregateID,
		Handle:      i.Handle,
		Payload:     i.Payload,
		CreatedAt:   i.CreatedAt,
	}
}

func (ais *AggregateInteractions) Apply(ai AggregateInteraction) error {
	logger.Infof("ais %v", ais)
	handlers := strings.Split(ai.Handle, ".")
	switch handlers[0] {
	case "Sex":
		payload, err := ai.ParseSexPayload()
		if err != nil {
			return err
		}
		ais.Sex = &payload.Value
	case "Room":
		if handlers[1] == "Init" {

		}
	default:
		return fmt.Errorf("bad handler: %v", ai.Handle)
	}
	ais.Interactions = append(ais.Interactions, ai)
	return nil
}

type SexPayoload struct {
	Value infermedica.Sex `json:"value"`
}

func (ai AggregateInteraction) ParseSexPayload() (*SexPayoload, error) {
	p := SexPayoload{}
	if ai.Payload == nil {
		return nil, errors.New("payload is nil")
	}
	err := json.Unmarshal([]byte(*ai.Payload), &p)
	if err != nil {
		return nil, err
	}
	return &p, nil
}
