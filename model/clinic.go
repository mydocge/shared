package model

import (
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
)

// Clinic is struct for moving clinic data between micro-services and frontend
type Clinic struct {
	ID        uuid.UUID `json:"id"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ClinicLog struct {
	Clinic      UserPubInfo              `json:"clinic"`
	Aggregate   constant.ClinicAggregate `json:"aggregate"`
	AggregateID string                   `json:"aggregate_id"`
	Handle      string                   `json:"handle"`
	HandleID    string                   `json:"handle_id"`
	Payload     string                   `json:"payload"`
	CreatedBy   UserPubInfo              `json:"created_by"`
	CreatedAt   time.Time                `json:"created_at"`
}

type ClinicLogs []ClinicLog

type ClinicServices struct {
	PersonalizedServices    map[string]PersonalizedService    `json:"personalized_services"`
	NonPersonalizedServices map[string]NonPersonalizedService `json:"non_personalized_services"`
}
type PersonalizedService struct {
	ServiceID string               `json:"service_id"`
	Doctors   map[uuid.UUID]string `json:"doctors"`
}

type NonPersonalizedService struct {
	ServiceID string `json:"service_id"`
	Price     string `json:"price"`
}
