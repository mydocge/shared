package model

import (
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/util"
)

type InfoNotification struct {
	ID         uuid.UUID           `json:"id"`
	UserID     uuid.UUID           `json:"user_id"`
	Actor      UserPubInfo         `json:"actor"`
	EntityID   uuid.UUID           `json:"entity_id"`
	EntityType constant.EntityType `json:"entity_type"`
	ActionType constant.ActionType `json:"action_type"`
	VisitedAt  *time.Time          `json:"visited_at"`
	SeenAt     *time.Time          `json:"seen_at"`
	CreatedAt  time.Time           `json:"created_at"`
}

func (in *InfoNotification) Message(to []string) Message {
	if in == nil {
		return Message{}
	}
	msg := Message{
		To: to,
		Data: MessageInfo{
			Type:    constant.MessageTypeInfoNotification,
			Payload: *in,
		},
	}
	return msg
}

type InfoNotificationCreateRequest struct {
	UserID     uuid.UUID           `json:"user_id"`
	ActorID    uuid.UUID           `json:"actor_id"`
	ActorType  constant.ActorType  `json:"actor_type"`
	EntityID   uuid.UUID           `json:"entity_id"`
	EntityType constant.EntityType `json:"entity_type"`
	ActionType constant.ActionType `json:"action_type"`
}

func (incr InfoNotificationCreateRequest) Validate() error {
	if incr.UserID == uuid.FromStringOrNil("") {
		return util.ErrorBadRequest{Message: "bad user id"}
	}
	if incr.ActorID == uuid.FromStringOrNil("") {
		return util.ErrorBadRequest{Message: "bad actor id"}
	}
	if incr.EntityID == uuid.FromStringOrNil("") {
		return util.ErrorBadRequest{Message: "bad entity id"}
	}

	return nil
}
