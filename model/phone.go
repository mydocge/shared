package model

import (
	"time"

	"github.com/satori/go.uuid"
)

// Phone is struct for moving phone data between micro-services and frontend
type Phone struct {
	ID          uuid.UUID `json:"id"`
	CountryCode string    `json:"country_code"`
	Number      string    `json:"number"`
	OwnerID     uuid.UUID `json:"user_id"`
	IsConfirmed bool      `json:"is_confirmed"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}
