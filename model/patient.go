package model

import (
	"time"

	"github.com/satori/go.uuid"
	"github.com/torniker/infermedica"
	"gitlab.com/mydocge/shared/constant"
)

// PatientProfile describes profile info that is stored for patient user
type PatientProfile struct {
	ID                 uuid.UUID `json:"id"`
	HasDiabetes        string    `json:"has_diabetes"`
	HasHighCholesterol string    `json:"has_high_cholesterol"`
	HasHypertension    string    `json:"has_hypertension"`
	IsSmoker           string    `json:"is_smoker"`
	CreatedAt          time.Time `json:"created_at"`
	UpdatedAt          time.Time `json:"updated_at"`
}

func (pp PatientProfile) EvidencesMap() EvidencesMap {
	eMap := EvidencesMap{}
	if pp.HasDiabetes != "" {
		eMap["diabetes"] = Evidence{
			ID:       "diabetes",
			ChoiceID: pp.HasDiabetes,
		}
	}
	if pp.HasHighCholesterol != "" {
		eMap["high_cholesterol"] = Evidence{
			ID:       "high_cholesterol",
			ChoiceID: pp.HasHighCholesterol,
		}
	}
	if pp.HasHypertension != "" {
		eMap["hypertension"] = Evidence{
			ID:       "hypertension",
			ChoiceID: pp.HasHypertension,
		}
	}
	if pp.IsSmoker != "" {
		eMap["smoking"] = Evidence{
			ID:       "smoking",
			ChoiceID: pp.IsSmoker,
		}
	}
	return eMap
}

type PatientRisksInfo struct {
	Sex             string       `json:"sex"`
	RiskEvidenceMap EvidencesMap `json:"risk_evidence_map"`
}

// Patient is struct for moving patient data between micro-services and frontend
type Patient struct {
	User
	// Logs []schema.PatientLog `json:"logs"`
	// Agregate Objects
	// Rel *map[uuid.UUID]Rel `json:"room"`
	// Services *map[uuid.UUID]Service       `json:"services"`
	// Age      *int             `json:"age"`
	Sex *infermedica.Sex `json:"sex"`
	// Height   *int             `json:"height"`
	// Weight   *int             `json:"weight"`
}

type PatientLog struct {
	Patient     UserPubInfo        `json:"patient"`
	Aggregate   constant.Aggregate `json:"aggregate"`
	AggregateID string             `json:"aggregate_id"`
	Handle      string             `json:"handle"`
	HandleID    string             `json:"handle_id"`
	Payload     string             `json:"payload"`
	CreatedBy   UserPubInfo        `json:"created_by"`
	CreatedAt   time.Time          `json:"created_at"`
}

type PatientLogs []PatientLog

func (pl *PatientLog) Message(to []string) Message {
	if pl == nil {
		return Message{}
	}
	msg := Message{
		To: to,
		Data: MessageInfo{
			Type:    constant.MessageTypePatientLog,
			Payload: *pl,
		},
	}
	return msg
}

type PatientMessage struct {
	ID            uuid.UUID              `json:"id"`
	Patient       UserPubInfo            `json:"patient"`
	ChatSpaceType constant.ChatSpaceType `json:"chat_space_type"`
	ChatSpaceID   uuid.UUID              `json:"chat_space_id"`
	Text          string                 `json:"text"`
	CreatedBy     UserPubInfo            `json:"created_by"`
	CreatedAt     time.Time              `json:"created_at"`
}

type PatientMessages []PatientMessage

func (pm *PatientMessage) Message(to []string) Message {
	if pm == nil {
		return Message{}
	}
	msg := Message{
		To: to,
		Data: MessageInfo{
			Type:    constant.MessageTypePatientMessage,
			Payload: *pm,
		},
	}
	return msg
}
