package model

import "gitlab.com/mydocge/shared/util/common"

// Symptom describes symptom model stored in migration json for symptom service
type Symptom struct {
	ID       string              `json:"id"`
	Name     common.Translatable `json:"name"`
	Keywords common.Translatable `json:"keywords"`
	ICD10    string              `json:"icd10"`
	ICPC2    string              `json:"icpc2"`
}

type Symptoms []Symptom
