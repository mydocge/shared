package model

import (
	"encoding/json"
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/util"
)

type Room struct {
	ID        uuid.UUID   `json:"id"`
	Patient   UserPubInfo `json:"patient"`
	CreatedBy UserPubInfo `json:"created_by"`
	CreatedAt time.Time   `json:"created_at"`

	RoomUserLogs RoomUserLogs `json:"room_user_logs"`
}

type Rooms []Room

type RoomUserLog struct {
	Handle    constant.RULHandleType `json:"handle"`
	RoomID    uuid.UUID              `json:"room_id"`
	User      UserPubInfo            `json:"user"`
	Clinic    UserPubInfo            `json:"clinic"`
	CreatedBy UserPubInfo            `json:"created_by"`
	CreatedAt time.Time              `json:"created_at"`
}

type RoomUserLogs []RoomUserLog

type RoomLog struct {
	Handle    constant.RLHandleType `json:"handle"`
	RoomID    uuid.UUID             `json:"room_id"`
	Payload   *json.RawMessage      `json:"payload"`
	CreatedBy UserPubInfo           `json:"created_by"`
	CreatedAt time.Time             `json:"created_at"`
}

type RoomLogs []RoomLog

// MessagePayload describes payload field for handle type Message
type MessagePayload struct {
	Text string `json:"text"`
}

// ServiceInitPayload describes payload field for handle type ServiceInit
type ServiceInitPayload struct {
	ServiceID        uuid.UUID `json:"service_id"`
	CaseID           uuid.UUID `json:"case_id"`
	ProviderUserID   uuid.UUID `json:"provider_user_id"`
	ProviderClinicID uuid.UUID `json:"provider_clinic_id"`
}

// ServiceCommitPayload describes payload field for handle type ServiceCommit
type ServiceCommitPayload struct {
	CaseID uuid.UUID `json:"case_id"`
}

// EvidenceAddPayload describes payload field for handle type EvidenceAdd
type EvidenceAddPayload struct {
	CaseID   uuid.UUID `json:"case_id"`
	ID       string    `json:"id"`
	ChoiceID string    `json:"choice_id"`
}

// EvidenceRemovePayload describes payload field for handle type EvidenceRemove
type EvidenceRemovePayload struct {
	CaseID uuid.UUID `json:"case_id"`
	ID     string    `json:"id"`
}

// CaseRefAddPayload describes payload field for handle type CaseRefAdd
type CaseRefAddPayload struct {
	CaseID    uuid.UUID `json:"case_id"`
	RefCaseID uuid.UUID `json:"ref_case_id"`
}

// CaseRefRemovePayload describes payload field for handle type CaseRefRemove
type CaseRefRemovePayload struct {
	CaseID    uuid.UUID `json:"case_id"`
	RefCaseID uuid.UUID `json:"ref_case_id"`
}

// // Room is struct for moving room data between micro-services and frontend
// type Room struct {
// 	ID        uuid.UUID   `json:"id"`
// 	Patient   UserPubInfo `json:"patient"`
// 	User      UserPubInfo `json:"user"`
// 	Clinic    UserPubInfo `json:"clinic"`
// 	CreatedAt time.Time   `json:"created_at"`

// 	Logs []RoomLog `json:"logs"`

// 	ServiceCases   *map[uuid.UUID][]uuid.UUID          `json:"services"`
// 	Cases          *map[uuid.UUID]Case                 `json:"cases"`
// 	CurrentCaseID  *uuid.UUID                          `json:"current_case_id"`
// 	CaseEvidences  *map[uuid.UUID]map[string]Evidence  `json:"case_evidences"`
// 	CaseConditions *map[uuid.UUID]map[string]Condition `json:"case_conditions"`
// 	CaseBots       *map[uuid.UUID]map[uuid.UUID]bool   `json:"case_bots"`

// 	//current case id  -> combination of aggreagate id + handle (service init_)
// 	// cases id, start date,
// 	//case evidences id : evidence and all( no service all the rest)  ... by handle verbs
// 	//.. case files and etc

// }

type RoomLogAggregate struct {
	RoomID    uuid.UUID          `json:"id"`
	PatientID uuid.UUID          `json:"patient_id"`
	UsersMap  map[uuid.UUID]bool `json:"users_map"`
	CreatedAt time.Time          `json:"created_at"`

	Cases         map[uuid.UUID]Case                `json:"cases"`
	CaseEvidences map[uuid.UUID]map[string]Evidence `json:"case_evidences"`
	CaseRefCases  map[uuid.UUID]map[uuid.UUID]bool  `json:"case_ref_cases"`
}

// ApplyLog applies change to the list of changes and changes the state
func (rla *RoomLogAggregate) ApplyLog(rl RoomLog) error {
	switch rl.Handle {
	case constant.RLHandleTypeMessage:
		// check permission
		isPatient := false
		if rl.CreatedBy.ID == rla.PatientID {
			isPatient = true
		}
		isUser := false
		if val, ok := rla.UsersMap[rl.CreatedBy.ID]; ok {
			isUser = val
		}
		if !isPatient && !isUser {
			return util.ErrorStatusNotAllowed{}
		}

	case constant.RLHandleTypeServiceInit:
		// parse payload
		if rl.Payload == nil {
			return util.ErrorBadRequest{Message: "no payload - service init"}
		}
		sip := ServiceInitPayload{}
		err := json.Unmarshal(*rl.Payload, &sip)
		if err != nil {
			return util.ErrorBadRequest{Message: "bad payload - service init"}
		}
		//handle add to cases
		if _, ok := rla.Cases[sip.CaseID]; ok {
			return util.ErrorBadRequest{Message: "case already initiated - service init"}
		}
		rla.Cases[sip.CaseID] = Case{
			ID:               sip.CaseID,
			ServiceID:        sip.ServiceID,
			ProviderUserID:   sip.ProviderUserID,
			ProviderClinicID: sip.ProviderClinicID,
			InitDate:         rla.CreatedAt,
			CommitDates:      []time.Time{},
		}
	case constant.RLHandleTypeServiceCommit:
		// parse payload
		if rl.Payload == nil {
			return util.ErrorBadRequest{Message: "no payload - service commit"}
		}
		scp := ServiceCommitPayload{}
		err := json.Unmarshal(*rl.Payload, &scp)
		if err != nil {
			return util.ErrorBadRequest{Message: "bad payload - service commit"}
		}
		//handle add to cases
		if val, ok := rla.Cases[scp.CaseID]; !ok {
			return util.ErrorBadRequest{Message: "case without init - service commit"}
		} else {
			val.CommitDates = append(val.CommitDates, time.Now())
			rla.Cases[scp.CaseID] = val
		}
	case constant.RLHandleTypeEvidenceAdd:
		// parse payload
		if rl.Payload == nil {
			return util.ErrorBadRequest{Message: "no payload - evidence add"}
		}
		eap := EvidenceAddPayload{}
		err := json.Unmarshal(*rl.Payload, &eap)
		if err != nil {
			return util.ErrorBadRequest{Message: "bad payload - evidence add"}
		}
		//check case exists
		if _, ok := rla.Cases[eap.CaseID]; !ok {
			return util.ErrorBadRequest{Message: "case not found - evidence add"}
		}

		if _, ok := rla.CaseEvidences[eap.CaseID]; !ok {
			rla.CaseEvidences[eap.CaseID] = make(map[string]Evidence)
		}
		val, _ := rla.CaseEvidences[eap.CaseID]
		val[eap.ID] = Evidence{
			ID:       eap.ID,
			ChoiceID: eap.ChoiceID,
		}
		rla.CaseEvidences[eap.CaseID] = val
	case constant.RLHandleTypeEvidenceRemove:
		// parse payload
		if rl.Payload == nil {
			return util.ErrorBadRequest{Message: "no payload - evidence remove"}
		}
		erp := EvidenceRemovePayload{}
		err := json.Unmarshal(*rl.Payload, &erp)
		if err != nil {
			return util.ErrorBadRequest{Message: "bad payload - evidence remove"}
		}
		//check case exists
		if _, ok := rla.Cases[erp.CaseID]; !ok {
			return util.ErrorBadRequest{Message: "case not found - evidence remove"}
		}
		if _, ok := rla.CaseEvidences[erp.CaseID]; !ok {
			return util.ErrorBadRequest{Message: "no case evidences - evidence remove"}
		}
		if _, ok := rla.CaseEvidences[erp.CaseID][erp.ID]; !ok {
			return util.ErrorBadRequest{Message: "evidence not found - evidence remove"}
		}
		delete(rla.CaseEvidences[erp.CaseID], erp.ID)

	}
	return rla.validate()
}

// validates room aggregate state after applying
func (rla *RoomLogAggregate) validate() error {
	return nil
}

// // RoomLog is a struct for Room changes
// type RoomLog struct {
// 	ID          uuid.UUID             `json:"id"`
// 	RoomID      uuid.UUID             `json:"room_id"`
// 	Author      constant.AuthorType   `json:"author"`
// 	AggregateID string                `json:"aggregate_id"`
// 	Handle      constant.RLHandleType `json:"handle"`
// 	CaseID      *uuid.UUID            `json:"case_id"`
// 	Payload     *json.RawMessage      `json:"payload"`
// 	CreatedAt   time.Time             `json:"created_at"`
// }

// // Message converts room log to message for pusher service
// func (rl *RoomLog) Message(to []uuid.UUID) Message {
// 	if rl == nil {
// 		return Message{}
// 	}
// 	msg := Message{
// 		To: make(map[uuid.UUID]bool),
// 		Data: MessageInfo{
// 			Type:    constant.MessageTypeRoomLog,
// 			Payload: *rl,
// 		},
// 	}
// 	for _, id := range to {
// 		msg.To[id] = true
// 	}
// 	return msg
// }
