package model

import (
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/util/common"
)

type Bot struct {
	ID         uuid.UUID           `json:"id"`
	Name       common.Translatable `json:"name"`
	UpdateLink string              `json:"update_link"`
	CreatedAt  time.Time           `json:"created_at"`
}

type Bots []Bot

type CaseBot struct {
	IsActive bool `json:"is_active"`
}
