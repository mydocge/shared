package model

import (
	"time"

	"github.com/satori/go.uuid"
)

type Case struct {
	ID               uuid.UUID   `json:"id"`
	ServiceID        uuid.UUID   `json:"service_id"`
	ProviderUserID   uuid.UUID   `json:"provider_user_id"`
	ProviderClinicID uuid.UUID   `json:"provider_clinic_id"`
	InitDate         time.Time   `json:"init_date"`
	CommitDates      []time.Time `json:"commit_dates"`
}

type Cases []Case
