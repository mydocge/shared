package model

import (
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/util/common"
)

// User is struct for moving user data between micro-services and frontend
type User struct {
	ID         uuid.UUID           `json:"id"`
	Username   string              `json:"username"`
	Password   string              `json:"-"`
	FirstName  common.Translatable `json:"first_name"`
	LastName   common.Translatable `json:"last_name"`
	Photo      string              `json:"photo"`
	Country    string              `json:"country"`
	Identifier string              `json:"identifier"`
	Gender     string              `json:"gender"`
	Birthdate  *time.Time          `json:"birthdate"`
	Language   constant.Language   `json:"language"`
	Languages  []constant.Language `json:"languages"`
	Email      *Email              `json:"email"`
	Emails     *[]Email            `json:"emails"`
	Phone      *Phone              `json:"phone"`
	Phones     *[]Phone            `json:"phones"`
	Type       constant.UserType   `json:"type"`
	ClinicID   *uuid.UUID          `json:"clinic_id"`
	CreatedAt  time.Time           `json:"created_at"`
	DeletedAt  *time.Time          `json:"deleted_at"`
	UpdatedAt  time.Time           `json:"updated_at"`
}

// UserPubInfo shortened version of User model for public user lookup
type UserPubInfo struct {
	ID    uuid.UUID           `json:"id"`
	Name  common.Translatable `json:"name"`
	Type  constant.UserType   `json:"type"`
	Photo string              `json:"photo"`
}

// PubInfo generates public info struct from user model
func (u *User) PubInfo() UserPubInfo {
	if u == nil {
		return UserPubInfo{}
	}
	upi := UserPubInfo{
		ID:    (*u).ID,
		Photo: (*u).Photo,
		Type:  (*u).Type,
		Name:  make(map[constant.Language]string),
	}
	for _, lang := range u.Languages {
		upi.Name[lang] = u.FirstName[lang] + " " + u.LastName[lang]
	}

	return upi
}

// UserCreateResponse describes user create result
type UserCreateResponse struct {
	User *User `json:"user"`
	Code int   `json:"code"`
	Err  error `json:"err"`
}

// UserInfoRequest describes params for user info request via NATS
type UserInfoRequest struct {
	ID         uuid.UUID         `json:"id"`
	Email      string            `json:"email"`
	Identifier string            `json:"identifier"`
	Token      string            `json:"token"`
	ClinicID   uuid.UUID         `json:"clinic_id"`
	RoomID     uuid.UUID         `json:"room_id"`
	Type       constant.UserType `json:"type"`
	Language   constant.Language `json:"language"`
}

type UserSocketParams struct {
	Token       string `json:"token"`
	SocketID    string `json:"socket_id"`
	IsConnected bool   `json:"is_connected"`
}

func (u *User) Message(to []string) Message {
	if u == nil {
		return Message{}
	}
	msg := Message{
		To: to,
		Data: MessageInfo{
			Type:    constant.MessageTypeUserInfoUpdate,
			Payload: *u,
		},
	}
	return msg
}
