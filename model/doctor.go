package model

import (
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/util/common"
)

// DoctorProfile describes doctor's profile information
type DoctorProfile struct {
	ID               uuid.UUID           `json:"id"`
	Specialization   string              `json:"specialization"`
	CountryOfService string              `json:"country_of_service"`
	Since            string              `json:"since"`
	Description      common.Translatable `json:"description"`
	Title            common.Translatable `json:"title"`
	CreatedAt        time.Time           `json:"created_at"`
	UpdatedAt        time.Time           `json:"updated_at"`
}
