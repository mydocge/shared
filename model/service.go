package model

import (
	"time"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
)

// Service is struct for moving service data between micro-services and frontend
type Service struct {
	ID          uuid.UUID                `json:"id"`
	Name        string                   `json:"name"`
	Description string                   `json:"description"`
	Category    constant.ServiceCategory `json:"category"`
	CreatedAt   time.Time                `json:"created_at"`
	UpdatedAt   time.Time                `json:"updated_at"`
}
