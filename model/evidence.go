package model

// Evidence contains evidence ID and choice (present, absent, unknown)
type Evidence struct {
	ID       string `json:"id"`
	ChoiceID string `json:"choice_id"`
	Initial  bool   `json:"initial"`
}

// Evidences is slice of evidence
type Evidences []Evidence

type EvidencesMap map[string]Evidence
