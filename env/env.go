package env

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
	"github.com/olivere/elastic"
	"github.com/torniker/infermedica"
	"gitlab.com/mydocge/shared/constant"

	"github.com/jmoiron/sqlx"
	"github.com/kelseyhightower/envconfig"
	nats "github.com/nats-io/go-nats"
	"github.com/tinrab/retry"
	"gitlab.com/mydocge/shared/util/logger"
)

// Env type
type Env string

// Possible Env values
const (
	Production  Env = "Production"
	Testing     Env = "Testing"
	Development Env = "Development"
)

type environment struct {
	value       *Env
	lang        *constant.Language
	db          *sqlx.DB
	nc          *nats.Conn
	es          *elastic.Client
	redis       *redis.Client
	infermedica *infermedica.App
}

var e = environment{}

func SetLang(l constant.Language) {
	e.lang = &l
}

func Lang() constant.Language {
	if e.lang == nil {
		l := constant.LanguageKa
		e.lang = &l
	}
	return *e.lang
}

type EnvConfig struct {
	Environment Env `envconfig:"ENV"`
}

// Get returns the value for env
func Get() Env {
	if e.value != nil {
		return *e.value
	}
	var cfg EnvConfig
	err := envconfig.Process("", &cfg)
	if err != nil {
		panic(err)
	}
	e.value = &cfg.Environment
	return *e.value
}

type PostgresConfig struct {
	PostgresDB       string `envconfig:"POSTGRES_DB"`
	PostgresUser     string `envconfig:"POSTGRES_USER"`
	PostgresPassword string `envconfig:"POSTGRES_PASSWORD"`
}

// GetPostgresDB returns postgres DB
func GetPostgresDB() (*sqlx.DB, error) {
	if e.db != nil {
		return e.db, nil
	}
	var cfg PostgresConfig
	err := envconfig.Process("", &cfg)
	if err != nil {
		return nil, err
	}
	retry.ForeverSleep(2*time.Second, func(attempt int) error {
		addr := fmt.Sprintf("postgres://%s:%s@postgres/%s?sslmode=disable", cfg.PostgresUser, cfg.PostgresPassword, cfg.PostgresDB)
		db, err := sqlx.Connect("postgres", addr)
		// db, err := sql.Open("postgres", addr)
		if err != nil {
			logger.Warnf("Counld not connect to DB on addr: %v", addr)
			return err
		}
		err = db.Ping()
		if err != nil {
			logger.Warnf("Counld not ping the DB")
			return err
		}
		e.db = db
		return nil
	})
	return e.db, nil
}

type ElasticConfig struct {
	ElasticURL string `envconfig:"ELASTICSEARCH_ADDRESS"`
}

// GetElastic returns elastic client
func GetElastic() (*elastic.Client, error) {
	if e.es != nil {
		return e.es, nil
	}
	var cfg ElasticConfig
	err := envconfig.Process("", &cfg)
	if err != nil {
		return nil, err
	}
	retry.ForeverSleep(2*time.Second, func(_ int) error {
		client, err := elastic.NewClient(
			elastic.SetURL(cfg.ElasticURL),
			elastic.SetSniff(false),
		)
		if err != nil {
			logger.Warnf("---- %v", cfg.ElasticURL)
			logger.Warnf("Counld not connect to ES: %v", err)
			return err
		}
		e.es = client
		return nil
	})
	return e.es, nil
}

type RedisConfig struct {
	RedisAddr     string `envconfig:"REDIS_ADDR"`
	RedisPassword string `envconfig:"REDIS_PASSWORD"`
}

func GetRedis() (*redis.Client, error) {
	if e.redis != nil {
		return e.redis, nil
	}
	var cfg RedisConfig
	err := envconfig.Process("", &cfg)
	if err != nil {
		return nil, err
	}
	retry.ForeverSleep(2*time.Second, func(_ int) error {
		client := redis.NewClient(&redis.Options{
			Addr:     cfg.RedisAddr,
			Password: cfg.RedisPassword,
			DB:       0,
		})
		_, err := client.Ping().Result()
		if err != nil {
			return err
		}
		e.redis = client
		return nil
	})
	return e.redis, nil
}

type NatsConfig struct {
	NatsAddress string `envconfig:"NATS_ADDRESS"`
}

func GetNATSConn() (*nats.Conn, error) {
	if e.nc != nil {
		return e.nc, nil
	}
	var cfg NatsConfig
	err := envconfig.Process("", &cfg)
	if err != nil {
		return nil, err
	}
	retry.ForeverSleep(2*time.Second, func(_ int) error {
		nc, err := nats.Connect(cfg.NatsAddress)
		if err != nil {
			return err
		}
		e.nc = nc
		return nil
	})
	return e.nc, nil
}

// Infermedica returns instance of infermedica app
func Infermedica(interviewID string) *infermedica.App {
	if e.infermedica != nil {
		return e.infermedica
	}
	infApp := infermedica.NewApp("86f0b979", "4696358aeeac54304051d49cb05d7a9c", "infermedica-ka-symptom", interviewID)
	e.infermedica = &infApp

	return e.infermedica
}
