package util

type ErrorBadRequest struct {
	Message string
}

func (e ErrorBadRequest) Error() string {
	return e.Message
}

type ErrorStatusUnauthorized struct{}

func (e ErrorStatusUnauthorized) Error() string {
	return "unauthorized"
}

type ErrorStatusNotAllowed struct{}

func (e ErrorStatusNotAllowed) Error() string {
	return "not allowed"
}

type ErrorStatusNotFound struct{}

func (e ErrorStatusNotFound) Error() string {
	return "not found"
}

type ErrorInternalServerError struct {
	Message string
}

func (e ErrorInternalServerError) Error() string {
	return e.Message
}
