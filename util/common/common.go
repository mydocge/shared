package common

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"strings"

	"github.com/satori/go.uuid"
	"gitlab.com/mydocge/shared/constant"
)

type Translatable map[constant.Language]string

func (t Translatable) Value() (driver.Value, error) {
	bytes, err := json.Marshal(t)
	if err != nil {
		return nil, err
	}
	return driver.Value(string(bytes)), nil
}

func (t *Translatable) Scan(src interface{}) error {
	switch src.(type) {
	case []byte:
		return json.Unmarshal(src.([]byte), t)
	default:
		return errors.New("error")
	}
}

type UUIDs []uuid.UUID

func (uuids UUIDs) Value() (driver.Value, error) {
	var ids []string
	for _, id := range uuids {
		ids = append(ids, id.String())
	}
	return driver.Value("{" + strings.Join(ids, "},{") + "}"), nil
}

func (uuids *UUIDs) Scan(src interface{}) error {
	switch src.(type) {
	case []byte:
		srcStr := string(src.([]byte))
		srcStr = srcStr[1 : len(srcStr)-1]
		srcArr := strings.Split(srcStr, ",")
		for _, id := range srcArr {
			convertedID, err := uuid.FromString(id)
			if err != nil {
				return err
			}
			*uuids = append(*uuids, convertedID)
		}
	default:
		return errors.New("error")
	}
	return nil
}

type Languages []constant.Language

func (langs Languages) Value() (driver.Value, error) {
	var languages []string
	for _, lang := range langs {
		languages = append(languages, lang.String())
	}
	return driver.Value("{" + strings.Join(languages, ",") + "}"), nil
}

func (langs *Languages) Scan(src interface{}) error {
	switch src.(type) {
	case []byte:
		srcStr := string(src.([]byte))
		srcStr = srcStr[1 : len(srcStr)-1]
		srcArr := strings.Split(srcStr, ",")
		for _, language := range srcArr {
			convertedLang, err := constant.LanguageFromString(language)
			if err != nil {
				return err
			}
			*langs = append(*langs, *convertedLang)
		}
	default:
		return errors.New("error")
	}
	return nil
}
