package util

import (
	"encoding/json"
	"net/http"

	"gitlab.com/mydocge/shared/constant"
	"gitlab.com/mydocge/shared/util/logger"
)

type ResponseData struct {
	Data interface{} `json:"data"`
}
type ResponseErr struct {
	Error string `json:"error"`
}

func ResponseOk(w http.ResponseWriter, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	json.NewEncoder(w).Encode(ResponseData{Data: body})
}

func ResponseError(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", "application/json")
	switch err.(type) {
	case ErrorBadRequest:
		w.WriteHeader(http.StatusBadRequest)
	case ErrorStatusUnauthorized:
		w.WriteHeader(http.StatusUnauthorized)
	case ErrorStatusNotAllowed:
		w.WriteHeader(http.StatusMethodNotAllowed)
	case ErrorStatusNotFound:
		w.WriteHeader(http.StatusNotFound)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	logger.ErrorWithCaller(logger.Caller(), err.Error())
	body := ResponseErr{
		Error: err.Error(),
	}
	json.NewEncoder(w).Encode(body)
}

func LogErr(err error) error {
	logger.ErrorWithCaller(logger.Caller(), err)
	return err
}

func Token(r *http.Request) (string, error) {
	if r == nil || len(r.Header.Get("Authorization")) < 8 {
		return "", LogErr(&ErrorBadRequest{Message: "no token"})
	}
	return r.Header.Get("Authorization")[7:], nil
}

func Language(r *http.Request) (*constant.Language, error) {
	if r == nil || len(r.Header.Get("Language")) == 0 {
		return nil, LogErr(&ErrorBadRequest{Message: "no language"})
	}
	langStr := r.Header.Get("Language")
	lang, err := constant.LanguageFromString(langStr)
	if err != nil {
		return nil, err
	}
	return lang, nil
}
