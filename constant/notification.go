package constant

type ActorType int

const (
	ActorTypeUser   ActorType = 1
	ActorTypeClinic ActorType = 2
)

type EntityType string

const (
	EntityTypeSchedule  EntityType = "schedule"
	EntityTypeInterview EntityType = "interview"
	EntityTypeCase      EntityType = "case"
)

type ActionType string

const (
	ActionTypeCreate      ActionType = "create"
	ActionTypeUpdate      ActionType = "update"
	ActionTypeCancel      ActionType = "cancel"
	ActionTypeRemove      ActionType = "remove"
	ActionTypeRequest     ActionType = "request"
	ActionTypeComplete    ActionType = "complete"
	ActionTypePrediagnose ActionType = "prediagnose"
	ActionTypeFinish      ActionType = "finish"
)
