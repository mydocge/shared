package constant

type QuestionnaireOwnerType int

const (
	QuestionnaireOwnerTypeDoctor QuestionnaireOwnerType = 1
	QuestionnaireOwnerTypeClinic QuestionnaireOwnerType = 2
)

type QuestionnaireQuestionType string

const (
	QuestionnaireQuestionTypeYesNo        QuestionnaireQuestionType = "YesNo"
	QuestionnaireQuestionTypeYesNoUnknown QuestionnaireQuestionType = "YesNoUnknown"
)

func (qqt QuestionnaireQuestionType) String() string {
	return string(qqt)
}
