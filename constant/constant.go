package constant

import (
	"errors"
	"time"
)

// NatsTimeout duration for nats request
const NatsTimeout time.Duration = 500 * time.Millisecond

// EventType defines list of event keys for interaction via nats
type EventType string

const (
	// EventTypeAuthInfo - user auth status
	EventTypeAuthInfo EventType = "AuthInfo"
	// EventTypeBotInfoByID - bot object by id
	EventTypeBotInfoByID EventType = "BotInfoByID"
	// EventTypePatientInfoByID - user info by id
	EventTypePatientInfoByID EventType = "PatientInfoByID"
	// EventTypeUserInfoByID - users pub infos by ids
	EventTypeUserInfoByID EventType = "UserInfoByID"
	// EventTypeUsersByIdentifier - user by personal id
	EventTypeUsersByIdentifier EventType = "UsersByIdentifier"
	// EventTypeUserPubInfoByIDs - users pub infos by ids
	EventTypeUserPubInfoByIDs EventType = "UserPubInfoByIDs"
	// EventTypeUserFullInfoByIDs - users pub infos by ids
	EventTypeUserFullInfoByIDs EventType = "UserFullInfoByIDs"
	// EventTypeClinicPubInfoByIDs - clinic pub infos by ids
	EventTypeClinicPubInfoByIDs EventType = "ClinicPubInfoByIDs"
	// EventTypeClinicServicesByID - clinic services maps by clinic id
	EventTypeClinicServicesByID EventType = "ClinicServicesByID"
	// EventTypeUserInfoByEmail - user object byemail address
	EventTypeUserInfoByEmail EventType = "UserInfoByEmail"
	// EventTypeCreateUserRequest - user requested to be created
	EventTypeCreateUserRequest EventType = "CreateUserRequest"
	// EventTypeUserInterests - request user ids which user is interested
	EventTypeUserInterests EventType = "UserInterests"
	// EventTypeRoomCreated - room created event
	EventTypeRoomCreated EventType = "RoomCreated"
	// EventTypeRoomOtherUsers - room other user ids
	EventTypeRoomOtherUsers EventType = "RoomOtherUsers"
	// EventTypePush - pushes message to user by socket
	EventTypePush EventType = "Push"

	// EventTypeUserSocketUpdate - user socket connections update
	EventTypeUserSocketUpdate EventType = "UserSocketUpdate"
	// EventTypeUserSocketInfos - users' socket connections info
	EventTypeUserSocketInfos EventType = "UserSocketInfos"
	// EventTypeUserSocketInfoByUserID - user's socket connections info
	EventTypeUserSocketInfoByUserID EventType = "UserSocketInfoByUserID"

	// EventTypeOperatorsByClinicID - users pub infos by ids
	EventTypeOperatorsByClinicID EventType = "OperatorsByClinicID"

	// EventTypeUserTyping - user typing message in a room
	EventTypeUserTyping EventType = "UserTyping"

	// EventTypeUserCalling - user calling
	EventTypeUserCalling EventType = "UserCalling"

	// EventTypeDeleteUserByID - user calling
	EventTypeDeleteUserByID EventType = "DeleteUserByID"

	// EventTypeCreateNotificationRequest - notification requested to be created
	EventTypeCreateNotificationRequest EventType = "CreateNotificationRequest"
)

func (et EventType) String() string {
	return string(et)
}

// UserType defines list of user types
type UserType int

const (
	// UserTypeGuest default user type
	UserTypeGuest UserType = 0
	// UserTypePatient - Patient
	UserTypePatient UserType = 1
	// UserTypeDoctor - Doctor
	UserTypeDoctor UserType = 2
	// UserTypeClinicAdmin - ClinicAdmin
	UserTypeClinicAdmin UserType = 3
	// UserTypeClinicOperator - ClinicOperator
	UserTypeClinicOperator UserType = 4
	// UserTypeBot - Bot
	UserTypeBot UserType = 100
	// UserTypeClinic - Clinic
	UserTypeClinic UserType = 99
)

// MessageType defines a list of push message types
type MessageType string

const (
	// MessageTypePatientLog - patient log
	MessageTypePatientLog MessageType = "PatientLog"
	// MessageTypePatientAggregate - patient aggregate
	MessageTypePatientAggregate MessageType = "PatientAggregate"
	// MessageTypeClinicInquiry - clinic inquiry aggregate
	MessageTypeClinicInquiry MessageType = "ClinicInquiry"
	// MessageTypeUserStatus - user status change (online/offline)
	MessageTypeUserStatus MessageType = "UserStatus"
	// MessageTypeUserTyping - user typing event
	MessageTypeUserTyping MessageType = "UserTyping"
	// MessageTypeCalling - user calling event
	MessageTypeCalling MessageType = "UserCalling"

	// MessageTypeUserInfoUpdate - user info update event
	MessageTypeUserInfoUpdate MessageType = "UserInfoUpdate"

	// MessageTypeInfoNotification - new info notification
	MessageTypeInfoNotification MessageType = "InfoNotification"
	// MessageTypeInfoNotification - new patient message created
	MessageTypePatientMessage MessageType = "PatientMessage"
)

// IncomingActionType defines a list of push message types
type IncomingActionType string

const (
	// IncomingActionTypeFocusChange - user focus change (focused/unfocused)
	IncomingActionTypeFocusChange IncomingActionType = "FocusChange"
	// IncomingActionTyping - user typing event
	IncomingActionTyping IncomingActionType = "Typing"
	// IncomingActionTyping - user calling event
	IncomingActionTypingCall IncomingActionType = "Call"
)

// CompanyUserType defines a list of company user types
type CompanyUserType string

const (
	// CompanyUserTypeOperator - company's operator
	CompanyUserTypeOperator CompanyUserType = "Operator"
	// CompanyUserTypeManagement - company's management representative
	CompanyUserTypeManagement CompanyUserType = "Management"
)

// McaseUserPerm defines list of user permission types for mcase
type McaseUserPerm string

const (
	// McaseUserPermPrimaryDoctor = primary doctor with max permissions for mcase
	McaseUserPermPrimaryDoctor McaseUserPerm = "PrimaryDoctor"
)

// PatientLogAuthorType defines a type of the record's owner
type AuthorType int

const (
	// PatientLogAuthorTypePatient - patient
	AuthorTypePatient AuthorType = 1
	// AuthorTypeUser - user
	AuthorTypeUser AuthorType = 2
	// AuthorTypeApplication - application (system)
	AuthorTypeApplication AuthorType = 4
)

type HandleType string

const (
	HandleTypeClinicDoctorBind HandleType = "ClinicDoctorBind"
)

// RLHandleType describes options for room logs handle property
type RLHandleType string

const (
	RLHandleTypeEvidenceAdd    RLHandleType = "EvidenceAdd"
	RLHandleTypeEvidenceRemove RLHandleType = "EvidenceRemove"
	RLHandleTypeCaseRefAdd     RLHandleType = "CaseRefAdd"
	RLHandleTypeCaseRefRemove  RLHandleType = "CaseRefRemove"
	RLHandleTypeMessage        RLHandleType = "Message"
	RLHandleTypeServiceInit    RLHandleType = "ServiceInit"
	RLHandleTypeServiceCommit  RLHandleType = "ServiceCommit"
	RLHandleTypeBot            RLHandleType = "Bot"
)

func RLHandleTypeFromString(str string) (*RLHandleType, error) {
	switch str {
	case "EvidenceAdd":
		e := RLHandleTypeEvidenceAdd
		return &e, nil
	case "EvidenceRemove":
		e := RLHandleTypeEvidenceRemove
		return &e, nil
	case "Message":
		e := RLHandleTypeMessage
		return &e, nil
	case "ServiceInit":
		e := RLHandleTypeServiceInit
		return &e, nil
	case "ServiceCommit":
		e := RLHandleTypeServiceCommit
		return &e, nil
	case "Bot":
		e := RLHandleTypeBot
		return &e, nil
	case "CaseRefAdd":
		e := RLHandleTypeCaseRefAdd
		return &e, nil
	case "CaseRefRemove":
		e := RLHandleTypeCaseRefRemove
		return &e, nil

	default:
		return nil, errors.New("bad handle type")
	}
}

// RULHandleType describes options for room user logs handle property
type RULHandleType string

const (
	RULHandleTypeAdd    RULHandleType = "Add"
	RULHandleTypeRemove RULHandleType = "Remove"
)

type QuestionStage string

const (
	QuestionStageGreeting             QuestionStage = "Greeting"
	QuestionStageServices             QuestionStage = "Services"
	QuestionStageGender               QuestionStage = "Gender"
	QuestionStageAge                  QuestionStage = "Age"
	QuestionStageInitial              QuestionStage = "Initial"
	QuestionStageRiskFactors          QuestionStage = "RiskFactors"
	QuestionStageSuggestions          QuestionStage = "Suggestions"
	QuestionStageInterview            QuestionStage = "Interview"
	QuestionStageDoctorInterview      QuestionStage = "DoctorInterview"
	QuestionStageSendToDoctor         QuestionStage = "SendToDoctor"
	QuestionStageDoctors              QuestionStage = "Doctors"
	QuestionStageDoctorsDirect        QuestionStage = "DoctorsDirect"
	QuestionStageServiceSuggestion    QuestionStage = "ServiceSuggestion"
	QuestionStageConfirmCheckerBreak  QuestionStage = "ConfirmCheckerBreak"
	QuestionStageRedirect             QuestionStage = "Redirect"
	QuestionStageFinish               QuestionStage = "Finish"
	QuestionStageCreateSymptomChecker QuestionStage = "CreateSymptomChecker"
	QuestionStageReplayGreeting       QuestionStage = "ReplayGreeting"
)

type QuestionType string

const (
	QuestionTypeContinue             QuestionType = "Continue"
	QuestionTypeSingle               QuestionType = "Single"
	QuestionTypeSingleSimple         QuestionType = "SingleSimple"
	QuestionTypeGender               QuestionType = "Gender"
	QuestionTypeAge                  QuestionType = "Age"
	QuestionTypeSymptom              QuestionType = "Symptom"
	QuestionTypeSendToDoctor         QuestionType = "SendToDoctor"
	QuestionTypeDoctorChoice         QuestionType = "DoctorChoice"
	QuestionTypeDoctorDirect         QuestionType = "DoctorDirect"
	QuestionTypeServiceSuggestion    QuestionType = "ServiceSuggestion"
	QuestionTypeConfirmCheckerBreak  QuestionType = "ConfirmCheckerBreak"
	QuestionTypeSelf                 QuestionType = "Self"
	QuestionTypeRedirect             QuestionType = "Redirect"
	QuestionTypeFinish               QuestionType = "Finish"
	QuestionTypeCreateSymptomChecker QuestionType = "CreateSymptomChecker"
	QuestionTypeReplayGreeting       QuestionType = "ReplayGreeting"
)

type Country string

const (
	CountryAfghanistan                  Country = "Afghanistan"
	CountryAlbania                      Country = "Albania"
	CountryAlgeria                      Country = "Algeria"
	CountryAmericanSamoa                Country = "American Samoa"
	CountryAndorra                      Country = "Andorra"
	CountryAngola                       Country = "Angola"
	CountryAnguilla                     Country = "Anguilla"
	CountryAntarctica                   Country = "Antarctica"
	CountryAntiguaAndBarbuda            Country = "Antigua and Barbuda"
	CountryArgentina                    Country = "Argentina"
	CountryArmenia                      Country = "Armenia"
	CountryAruba                        Country = "Aruba"
	CountryAustralia                    Country = "Australia"
	CountryAustria                      Country = "Austria"
	CountryAzerbaijan                   Country = "Azerbaijan"
	CountryBahamas                      Country = "Bahamas"
	CountryBahrain                      Country = "Bahrain"
	CountryBangladesh                   Country = "Bangladesh"
	CountryBarbados                     Country = "Barbados"
	CountryBelarus                      Country = "Belarus"
	CountryBelgium                      Country = "Belgium"
	CountryBelize                       Country = "Belize"
	CountryBenin                        Country = "Benin"
	CountryBermuda                      Country = "Bermuda"
	CountryBhutan                       Country = "Bhutan"
	CountryBolivia                      Country = "Bolivia"
	CountryBosniaAndHerzegovina         Country = "Bosnia and Herzegovina"
	CountryBotswana                     Country = "Botswana"
	CountryBrazil                       Country = "Brazil"
	CountryBritishIndianOceanTerritory  Country = "British Indian Ocean Territory"
	CountryBritishVirginIslands         Country = "British Virgin Islands"
	CountryBrunei                       Country = "Brunei"
	CountryBulgaria                     Country = "Bulgaria"
	CountryBurkinaFaso                  Country = "Burkina Faso"
	CountryBurundi                      Country = "Burundi"
	CountryCambodia                     Country = "Cambodia"
	CountryCameroon                     Country = "Cameroon"
	CountryCanada                       Country = "Canada"
	CountryCapeVerde                    Country = "Cape Verde"
	CountryCaymanIslands                Country = "Cayman Islands"
	CountryCentralAfricanRepublic       Country = "Central African Republic"
	CountryChad                         Country = "Chad"
	CountryChile                        Country = "Chile"
	CountryChina                        Country = "China"
	CountryChristmasIsland              Country = "Christmas Island"
	CountryCocosIslands                 Country = "Cocos Islands"
	CountryColombia                     Country = "Colombia"
	CountryComoros                      Country = "Comoros"
	CountryCookIslands                  Country = "Cook Islands"
	CountryCostaRica                    Country = "Costa Rica"
	CountryCroatia                      Country = "Croatia"
	CountryCuba                         Country = "Cuba"
	CountryCuracao                      Country = "Curacao"
	CountryCyprus                       Country = "Cyprus"
	CountryCzechRepublic                Country = "Czech Republic"
	CountryDemocraticRepublicOfTheCongo Country = "Democratic Republic of the Congo"
	CountryDenmark                      Country = "Denmark"
	CountryDjibouti                     Country = "Djibouti"
	CountryDominica                     Country = "Dominica"
	CountryDominicanRepublic            Country = "Dominican Republic"
	CountryEastTimor                    Country = "East Timor"
	CountryEcuador                      Country = "Ecuador"
	CountryEgypt                        Country = "Egypt"
	CountryElSalvador                   Country = "El Salvador"
	CountryEquatorialGuinea             Country = "Equatorial Guinea"
	CountryEritrea                      Country = "Eritrea"
	CountryEstonia                      Country = "Estonia"
	CountryEthiopia                     Country = "Ethiopia"
	CountryFalklandIslands              Country = "Falkland Islands"
	CountryFaroeIslands                 Country = "Faroe Islands"
	CountryFiji                         Country = "Fiji"
	CountryFinland                      Country = "Finland"
	CountryFrance                       Country = "France"
	CountryFrenchPolynesia              Country = "French Polynesia"
	CountryGabon                        Country = "Gabon"
	CountryGambia                       Country = "Gambia"
	CountryGeorgia                      Country = "Georgia"
	CountryGermany                      Country = "Germany"
	CountryGhana                        Country = "Ghana"
	CountryGibraltar                    Country = "Gibraltar"
	CountryGreece                       Country = "Greece"
	CountryGreenland                    Country = "Greenland"
	CountryGrenada                      Country = "Grenada"
	CountryGuam                         Country = "Guam"
	CountryGuatemala                    Country = "Guatemala"
	CountryGuernsey                     Country = "Guernsey"
	CountryGuinea                       Country = "Guinea"
	CountryGuineaBissau                 Country = "Guinea-Bissau"
	CountryGuyana                       Country = "Guyana"
	CountryHaiti                        Country = "Haiti"
	CountryHonduras                     Country = "Honduras"
	CountryHongKong                     Country = "Hong Kong"
	CountryHungary                      Country = "Hungary"
	CountryIceland                      Country = "Iceland"
	CountryIndia                        Country = "India"
	CountryIndonesia                    Country = "Indonesia"
	CountryIran                         Country = "Iran"
	CountryIraq                         Country = "Iraq"
	CountryIreland                      Country = "Ireland"
	CountryIsleOfMan                    Country = "Isle of Man"
	CountryIsrael                       Country = "Israel"
	CountryItaly                        Country = "Italy"
	CountryIvoryCoast                   Country = "Ivory Coast"
	CountryJamaica                      Country = "Jamaica"
	CountryJapan                        Country = "Japan"
	CountryJersey                       Country = "Jersey"
	CountryJordan                       Country = "Jordan"
	CountryKazakhstan                   Country = "Kazakhstan"
	CountryKenya                        Country = "Kenya"
	CountryKiribati                     Country = "Kiribati"
	CountryKosovo                       Country = "Kosovo"
	CountryKuwait                       Country = "Kuwait"
	CountryKyrgyzstan                   Country = "Kyrgyzstan"
	CountryLaos                         Country = "Laos"
	CountryLatvia                       Country = "Latvia"
	CountryLebanon                      Country = "Lebanon"
	CountryLesotho                      Country = "Lesotho"
	CountryLiberia                      Country = "Liberia"
	CountryLibya                        Country = "Libya"
	CountryLiechtenstein                Country = "Liechtenstein"
	CountryLithuania                    Country = "Lithuania"
	CountryLuxembourg                   Country = "Luxembourg"
	CountryMacau                        Country = "Macau"
	CountryMacedonia                    Country = "Macedonia"
	CountryMadagascar                   Country = "Madagascar"
	CountryMalawi                       Country = "Malawi"
	CountryMalaysia                     Country = "Malaysia"
	CountryMaldives                     Country = "Maldives"
	CountryMali                         Country = "Mali"
	CountryMalta                        Country = "Malta"
	CountryMarshallIslands              Country = "Marshall Islands"
	CountryMauritania                   Country = "Mauritania"
	CountryMauritius                    Country = "Mauritius"
	CountryMayotte                      Country = "Mayotte"
	CountryMexico                       Country = "Mexico"
	CountryMicronesia                   Country = "Micronesia"
	CountryMoldova                      Country = "Moldova"
	CountryMonaco                       Country = "Monaco"
	CountryMongolia                     Country = "Mongolia"
	CountryMontenegro                   Country = "Montenegro"
	CountryMontserrat                   Country = "Montserrat"
	CountryMorocco                      Country = "Morocco"
	CountryMozambique                   Country = "Mozambique"
	CountryMyanmar                      Country = "Myanmar"
	CountryNamibia                      Country = "Namibia"
	CountryNauru                        Country = "Nauru"
	CountryNepal                        Country = "Nepal"
	CountryNetherlands                  Country = "Netherlands"
	CountryNetherlandsAntilles          Country = "Netherlands Antilles"
	CountryNewCaledonia                 Country = "New Caledonia"
	CountryNewZealand                   Country = "New Zealand"
	CountryNicaragua                    Country = "Nicaragua"
	CountryNiger                        Country = "Niger"
	CountryNigeria                      Country = "Nigeria"
	CountryNiue                         Country = "Niue"
	CountryNorthKorea                   Country = "North Korea"
	CountryNorthernMarianaIslands       Country = "Northern Mariana Islands"
	CountryNorway                       Country = "Norway"
	CountryOman                         Country = "Oman"
	CountryPakistan                     Country = "Pakistan"
	CountryPalau                        Country = "Palau"
	CountryPalestine                    Country = "Palestine"
	CountryPanama                       Country = "Panama"
	CountryPapuaNewGuinea               Country = "Papua New Guinea"
	CountryParaguay                     Country = "Paraguay"
	CountryPeru                         Country = "Peru"
	CountryPhilippines                  Country = "Philippines"
	CountryPitcairn                     Country = "Pitcairn"
	CountryPoland                       Country = "Poland"
	CountryPortugal                     Country = "Portugal"
	CountryPuertoRico                   Country = "Puerto Rico"
	CountryQatar                        Country = "Qatar"
	CountryRepublicOfTheCongo           Country = "Republic of the Congo"
	CountryReunion                      Country = "Reunion"
	CountryRomania                      Country = "Romania"
	CountryRussia                       Country = "Russia"
	CountryRwanda                       Country = "Rwanda"
	CountrySaintBarthelemy              Country = "Saint Barthelemy"
	CountrySaintHelena                  Country = "Saint Helena"
	CountrySaintKittsAndNevis           Country = "Saint Kitts and Nevis"
	CountrySaintLucia                   Country = "Saint Lucia"
	CountrySaintMartin                  Country = "Saint Martin"
	CountrySaintPierreAndMiquelon       Country = "Saint Pierre and Miquelon"
	CountrySaintVincentAndTheGrenadines Country = "Saint Vincent and the Grenadines"
	CountrySamoa                        Country = "Samoa"
	CountrySanMarino                    Country = "San Marino"
	CountrySaoTomeAndPrincipe           Country = "Sao Tome and Principe"
	CountrySaudiArabia                  Country = "Saudi Arabia"
	CountrySenegal                      Country = "Senegal"
	CountrySerbia                       Country = "Serbia"
	CountrySeychelles                   Country = "Seychelles"
	CountrySierraLeone                  Country = "Sierra Leone"
	CountrySingapore                    Country = "Singapore"
	CountrySintMaarten                  Country = "Sint Maarten"
	CountrySlovakia                     Country = "Slovakia"
	CountrySlovenia                     Country = "Slovenia"
	CountrySolomonIslands               Country = "Solomon Islands"
	CountrySomalia                      Country = "Somalia"
	CountrySouthAfrica                  Country = "South Africa"
	CountrySouthKorea                   Country = "South Korea"
	CountrySouthSudan                   Country = "South Sudan"
	CountrySpain                        Country = "Spain"
	CountrySriLanka                     Country = "Sri Lanka"
	CountrySudan                        Country = "Sudan"
	CountrySuriname                     Country = "Suriname"
	CountrySvalbardAndJanMayen          Country = "Svalbard and Jan Mayen"
	CountrySwaziland                    Country = "Swaziland"
	CountrySweden                       Country = "Sweden"
	CountrySwitzerland                  Country = "Switzerland"
	CountrySyria                        Country = "Syria"
	CountryTaiwan                       Country = "Taiwan"
	CountryTajikistan                   Country = "Tajikistan"
	CountryTanzania                     Country = "Tanzania"
	CountryThailand                     Country = "Thailand"
	CountryTogo                         Country = "Togo"
	CountryTokelau                      Country = "Tokelau"
	CountryTonga                        Country = "Tonga"
	CountryTrinidadAndTobago            Country = "Trinidad and Tobago"
	CountryTunisia                      Country = "Tunisia"
	CountryTurkey                       Country = "Turkey"
	CountryTurkmenistan                 Country = "Turkmenistan"
	CountryTurksAndCaicosIslands        Country = "Turks and Caicos Islands"
	CountryTuvalu                       Country = "Tuvalu"
	CountryUSVirginIslands              Country = "U.S. Virgin Islands"
	CountryUganda                       Country = "Uganda"
	CountryUkraine                      Country = "Ukraine"
	CountryUnitedArabEmirates           Country = "United Arab Emirates"
	CountryUnitedKingdom                Country = "United Kingdom"
	CountryUnitedStates                 Country = "United States"
	CountryUruguay                      Country = "Uruguay"
	CountryUzbekistan                   Country = "Uzbekistan"
	CountryVanuatu                      Country = "Vanuatu"
	CountryVatican                      Country = "Vatican"
	CountryVenezuela                    Country = "Venezuela"
	CountryVietnam                      Country = "Vietnam"
	CountryWallisAndFutuna              Country = "Wallis and Futuna"
	CountryWesternSahara                Country = "Western Sahara"
	CountryYemen                        Country = "Yemen"
	CountryZambia                       Country = "Zambia"
	CountryZimbabwe                     Country = "Zimbabwe"
)

// CountryFromString returns const country based on string provided
func CountryFromString(country string) (*Country, error) {
	switch country {
	case "Afghanistan":
		c := CountryAfghanistan
		return &c, nil
	case "Albania":
		c := CountryAlbania
		return &c, nil
	case "Algeria":
		c := CountryAlgeria
		return &c, nil
	case "American Samoa":
		c := CountryAmericanSamoa
		return &c, nil
	case "Andorra":
		c := CountryAndorra
		return &c, nil
	case "Angola":
		c := CountryAngola
		return &c, nil
	case "Anguilla":
		c := CountryAnguilla
		return &c, nil
	case "Antarctica":
		c := CountryAntarctica
		return &c, nil
	case "Antigua and Barbuda":
		c := CountryAntiguaAndBarbuda
		return &c, nil
	case "Argentina":
		c := CountryArgentina
		return &c, nil
	case "Armenia":
		c := CountryArmenia
		return &c, nil
	case "Aruba":
		c := CountryAruba
		return &c, nil
	case "Australia":
		c := CountryAustralia
		return &c, nil
	case "Austria":
		c := CountryAustria
		return &c, nil
	case "Azerbaijan":
		c := CountryAzerbaijan
		return &c, nil
	case "Bahamas":
		c := CountryBahamas
		return &c, nil
	case "Bahrain":
		c := CountryBahrain
		return &c, nil
	case "Bangladesh":
		c := CountryBangladesh
		return &c, nil
	case "Barbados":
		c := CountryBarbados
		return &c, nil
	case "Belarus":
		c := CountryBelarus
		return &c, nil
	case "Belgium":
		c := CountryBelgium
		return &c, nil
	case "Belize":
		c := CountryBelize
		return &c, nil
	case "Benin":
		c := CountryBenin
		return &c, nil
	case "Bermuda":
		c := CountryBermuda
		return &c, nil
	case "Bhutan":
		c := CountryBhutan
		return &c, nil
	case "Bolivia":
		c := CountryBolivia
		return &c, nil
	case "Bosnia and Herzegovina":
		c := CountryBosniaAndHerzegovina
		return &c, nil
	case "Botswana":
		c := CountryBotswana
		return &c, nil
	case "Brazil":
		c := CountryBrazil
		return &c, nil
	case "British Indian Ocean Territory":
		c := CountryBritishIndianOceanTerritory
		return &c, nil
	case "British Virgin Islands":
		c := CountryBritishVirginIslands
		return &c, nil
	case "Brunei":
		c := CountryBrunei
		return &c, nil
	case "Bulgaria":
		c := CountryBulgaria
		return &c, nil
	case "Burkina Faso":
		c := CountryBurkinaFaso
		return &c, nil
	case "Burundi":
		c := CountryBurundi
		return &c, nil
	case "Cambodia":
		c := CountryCambodia
		return &c, nil
	case "Cameroon":
		c := CountryCameroon
		return &c, nil
	case "Canada":
		c := CountryCanada
		return &c, nil
	case "Cape Verde":
		c := CountryCapeVerde
		return &c, nil
	case "Cayman Islands":
		c := CountryCaymanIslands
		return &c, nil
	case "Central African Republic":
		c := CountryCentralAfricanRepublic
		return &c, nil
	case "Chad":
		c := CountryChad
		return &c, nil
	case "Chile":
		c := CountryChile
		return &c, nil
	case "China":
		c := CountryChina
		return &c, nil
	case "Christmas Island":
		c := CountryChristmasIsland
		return &c, nil
	case "Cocos Islands":
		c := CountryCocosIslands
		return &c, nil
	case "Colombia":
		c := CountryColombia
		return &c, nil
	case "Comoros":
		c := CountryComoros
		return &c, nil
	case "Cook Islands":
		c := CountryCookIslands
		return &c, nil
	case "Costa Rica":
		c := CountryCostaRica
		return &c, nil
	case "Croatia":
		c := CountryCroatia
		return &c, nil
	case "Cuba":
		c := CountryCuba
		return &c, nil
	case "Curacao":
		c := CountryCuracao
		return &c, nil
	case "Cyprus":
		c := CountryCyprus
		return &c, nil
	case "Czech Republic":
		c := CountryCzechRepublic
		return &c, nil
	case "Democratic Republic of the Congo":
		c := CountryDemocraticRepublicOfTheCongo
		return &c, nil
	case "Denmark":
		c := CountryDenmark
		return &c, nil
	case "Djibouti":
		c := CountryDjibouti
		return &c, nil
	case "Dominica":
		c := CountryDominica
		return &c, nil
	case "Dominican Republic":
		c := CountryDominicanRepublic
		return &c, nil
	case "East Timor":
		c := CountryEastTimor
		return &c, nil
	case "Ecuador":
		c := CountryEcuador
		return &c, nil
	case "Egypt":
		c := CountryEgypt
		return &c, nil
	case "El Salvador":
		c := CountryElSalvador
		return &c, nil
	case "Equatorial Guinea":
		c := CountryEquatorialGuinea
		return &c, nil
	case "Eritrea":
		c := CountryEritrea
		return &c, nil
	case "Estonia":
		c := CountryEstonia
		return &c, nil
	case "Ethiopia":
		c := CountryEthiopia
		return &c, nil
	case "Falkland Islands":
		c := CountryFalklandIslands
		return &c, nil
	case "Faroe Islands":
		c := CountryFaroeIslands
		return &c, nil
	case "Fiji":
		c := CountryFiji
		return &c, nil
	case "Finland":
		c := CountryFinland
		return &c, nil
	case "France":
		c := CountryFrance
		return &c, nil
	case "French Polynesia":
		c := CountryFrenchPolynesia
		return &c, nil
	case "Gabon":
		c := CountryGabon
		return &c, nil
	case "Gambia":
		c := CountryGambia
		return &c, nil
	case "Georgia":
		c := CountryGeorgia
		return &c, nil
	case "Germany":
		c := CountryGermany
		return &c, nil
	case "Ghana":
		c := CountryGhana
		return &c, nil
	case "Gibraltar":
		c := CountryGibraltar
		return &c, nil
	case "Greece":
		c := CountryGreece
		return &c, nil
	case "Greenland":
		c := CountryGreenland
		return &c, nil
	case "Grenada":
		c := CountryGrenada
		return &c, nil
	case "Guam":
		c := CountryGuam
		return &c, nil
	case "Guatemala":
		c := CountryGuatemala
		return &c, nil
	case "Guernsey":
		c := CountryGuernsey
		return &c, nil
	case "Guinea":
		c := CountryGuinea
		return &c, nil
	case "Guinea-Bissau":
		c := CountryGuineaBissau
		return &c, nil
	case "Guyana":
		c := CountryGuyana
		return &c, nil
	case "Haiti":
		c := CountryHaiti
		return &c, nil
	case "Honduras":
		c := CountryHonduras
		return &c, nil
	case "Hong Kong":
		c := CountryHongKong
		return &c, nil
	case "Hungary":
		c := CountryHungary
		return &c, nil
	case "Iceland":
		c := CountryIceland
		return &c, nil
	case "India":
		c := CountryIndia
		return &c, nil
	case "Indonesia":
		c := CountryIndonesia
		return &c, nil
	case "Iran":
		c := CountryIran
		return &c, nil
	case "Iraq":
		c := CountryIraq
		return &c, nil
	case "Ireland":
		c := CountryIreland
		return &c, nil
	case "Isle of Man":
		c := CountryIsleOfMan
		return &c, nil
	case "Israel":
		c := CountryIsrael
		return &c, nil
	case "Italy":
		c := CountryItaly
		return &c, nil
	case "Ivory Coast":
		c := CountryIvoryCoast
		return &c, nil
	case "Jamaica":
		c := CountryJamaica
		return &c, nil
	case "Japan":
		c := CountryJapan
		return &c, nil
	case "Jersey":
		c := CountryJersey
		return &c, nil
	case "Jordan":
		c := CountryJordan
		return &c, nil
	case "Kazakhstan":
		c := CountryKazakhstan
		return &c, nil
	case "Kenya":
		c := CountryKenya
		return &c, nil
	case "Kiribati":
		c := CountryKiribati
		return &c, nil
	case "Kosovo":
		c := CountryKosovo
		return &c, nil
	case "Kuwait":
		c := CountryKuwait
		return &c, nil
	case "Kyrgyzstan":
		c := CountryKyrgyzstan
		return &c, nil
	case "Laos":
		c := CountryLaos
		return &c, nil
	case "Latvia":
		c := CountryLatvia
		return &c, nil
	case "Lebanon":
		c := CountryLebanon
		return &c, nil
	case "Lesotho":
		c := CountryLesotho
		return &c, nil
	case "Liberia":
		c := CountryLiberia
		return &c, nil
	case "Libya":
		c := CountryLibya
		return &c, nil
	case "Liechtenstein":
		c := CountryLiechtenstein
		return &c, nil
	case "Lithuania":
		c := CountryLithuania
		return &c, nil
	case "Luxembourg":
		c := CountryLuxembourg
		return &c, nil
	case "Macau":
		c := CountryMacau
		return &c, nil
	case "Macedonia":
		c := CountryMacedonia
		return &c, nil
	case "Madagascar":
		c := CountryMadagascar
		return &c, nil
	case "Malawi":
		c := CountryMalawi
		return &c, nil
	case "Malaysia":
		c := CountryMalaysia
		return &c, nil
	case "Maldives":
		c := CountryMaldives
		return &c, nil
	case "Mali":
		c := CountryMali
		return &c, nil
	case "Malta":
		c := CountryMalta
		return &c, nil
	case "Marshall Islands":
		c := CountryMarshallIslands
		return &c, nil
	case "Mauritania":
		c := CountryMauritania
		return &c, nil
	case "Mauritius":
		c := CountryMauritius
		return &c, nil
	case "Mayotte":
		c := CountryMayotte
		return &c, nil
	case "Mexico":
		c := CountryMexico
		return &c, nil
	case "Micronesia":
		c := CountryMicronesia
		return &c, nil
	case "Moldova":
		c := CountryMoldova
		return &c, nil
	case "Monaco":
		c := CountryMonaco
		return &c, nil
	case "Mongolia":
		c := CountryMongolia
		return &c, nil
	case "Montenegro":
		c := CountryMontenegro
		return &c, nil
	case "Montserrat":
		c := CountryMontserrat
		return &c, nil
	case "Morocco":
		c := CountryMorocco
		return &c, nil
	case "Mozambique":
		c := CountryMozambique
		return &c, nil
	case "Myanmar":
		c := CountryMyanmar
		return &c, nil
	case "Namibia":
		c := CountryNamibia
		return &c, nil
	case "Nauru":
		c := CountryNauru
		return &c, nil
	case "Nepal":
		c := CountryNepal
		return &c, nil
	case "Netherlands":
		c := CountryNetherlands
		return &c, nil
	case "Netherlands Antilles":
		c := CountryNetherlandsAntilles
		return &c, nil
	case "New Caledonia":
		c := CountryNewCaledonia
		return &c, nil
	case "New Zealand":
		c := CountryNewZealand
		return &c, nil
	case "Nicaragua":
		c := CountryNicaragua
		return &c, nil
	case "Niger":
		c := CountryNiger
		return &c, nil
	case "Nigeria":
		c := CountryNigeria
		return &c, nil
	case "Niue":
		c := CountryNiue
		return &c, nil
	case "North Korea":
		c := CountryNorthKorea
		return &c, nil
	case "Northern Mariana Islands":
		c := CountryNorthernMarianaIslands
		return &c, nil
	case "Norway":
		c := CountryNorway
		return &c, nil
	case "Oman":
		c := CountryOman
		return &c, nil
	case "Pakistan":
		c := CountryPakistan
		return &c, nil
	case "Palau":
		c := CountryPalau
		return &c, nil
	case "Palestine":
		c := CountryPalestine
		return &c, nil
	case "Panama":
		c := CountryPanama
		return &c, nil
	case "Papua New Guinea":
		c := CountryPapuaNewGuinea
		return &c, nil
	case "Paraguay":
		c := CountryParaguay
		return &c, nil
	case "Peru":
		c := CountryPeru
		return &c, nil
	case "Philippines":
		c := CountryPhilippines
		return &c, nil
	case "Pitcairn":
		c := CountryPitcairn
		return &c, nil
	case "Poland":
		c := CountryPoland
		return &c, nil
	case "Portugal":
		c := CountryPortugal
		return &c, nil
	case "Puerto Rico":
		c := CountryPuertoRico
		return &c, nil
	case "Qatar":
		c := CountryQatar
		return &c, nil
	case "Republic of the Congo":
		c := CountryRepublicOfTheCongo
		return &c, nil
	case "Reunion":
		c := CountryReunion
		return &c, nil
	case "Romania":
		c := CountryRomania
		return &c, nil
	case "Russia":
		c := CountryRussia
		return &c, nil
	case "Rwanda":
		c := CountryRwanda
		return &c, nil
	case "Saint Barthelemy":
		c := CountrySaintBarthelemy
		return &c, nil
	case "Saint Helena":
		c := CountrySaintHelena
		return &c, nil
	case "Saint Kitts and Nevis":
		c := CountrySaintKittsAndNevis
		return &c, nil
	case "Saint Lucia":
		c := CountrySaintLucia
		return &c, nil
	case "Saint Martin":
		c := CountrySaintMartin
		return &c, nil
	case "Saint Pierre and Miquelon":
		c := CountrySaintPierreAndMiquelon
		return &c, nil
	case "Saint Vincent and the Grenadines":
		c := CountrySaintVincentAndTheGrenadines
		return &c, nil
	case "Samoa":
		c := CountrySamoa
		return &c, nil
	case "San Marino":
		c := CountrySanMarino
		return &c, nil
	case "Sao Tome and Principe":
		c := CountrySaoTomeAndPrincipe
		return &c, nil
	case "Saudi Arabia":
		c := CountrySaudiArabia
		return &c, nil
	case "Senegal":
		c := CountrySenegal
		return &c, nil
	case "Serbia":
		c := CountrySerbia
		return &c, nil
	case "Seychelles":
		c := CountrySeychelles
		return &c, nil
	case "Sierra Leone":
		c := CountrySierraLeone
		return &c, nil
	case "Singapore":
		c := CountrySingapore
		return &c, nil
	case "Sint Maarten":
		c := CountrySintMaarten
		return &c, nil
	case "Slovakia":
		c := CountrySlovakia
		return &c, nil
	case "Slovenia":
		c := CountrySlovenia
		return &c, nil
	case "Solomon Islands":
		c := CountrySolomonIslands
		return &c, nil
	case "Somalia":
		c := CountrySomalia
		return &c, nil
	case "South Africa":
		c := CountrySouthAfrica
		return &c, nil
	case "South Korea":
		c := CountrySouthKorea
		return &c, nil
	case "South Sudan":
		c := CountrySouthSudan
		return &c, nil
	case "Spain":
		c := CountrySpain
		return &c, nil
	case "Sri Lanka":
		c := CountrySriLanka
		return &c, nil
	case "Sudan":
		c := CountrySudan
		return &c, nil
	case "Suriname":
		c := CountrySuriname
		return &c, nil
	case "Svalbard and Jan Mayen":
		c := CountrySvalbardAndJanMayen
		return &c, nil
	case "Swaziland":
		c := CountrySwaziland
		return &c, nil
	case "Sweden":
		c := CountrySweden
		return &c, nil
	case "Switzerland":
		c := CountrySwitzerland
		return &c, nil
	case "Syria":
		c := CountrySyria
		return &c, nil
	case "Taiwan":
		c := CountryTaiwan
		return &c, nil
	case "Tajikistan":
		c := CountryTajikistan
		return &c, nil
	case "Tanzania":
		c := CountryTanzania
		return &c, nil
	case "Thailand":
		c := CountryThailand
		return &c, nil
	case "Togo":
		c := CountryTogo
		return &c, nil
	case "Tokelau":
		c := CountryTokelau
		return &c, nil
	case "Tonga":
		c := CountryTonga
		return &c, nil
	case "Trinidad and Tobago":
		c := CountryTrinidadAndTobago
		return &c, nil
	case "Tunisia":
		c := CountryTunisia
		return &c, nil
	case "Turkey":
		c := CountryTurkey
		return &c, nil
	case "Turkmenistan":
		c := CountryTurkmenistan
		return &c, nil
	case "Turks and Caicos Islands":
		c := CountryTurksAndCaicosIslands
		return &c, nil
	case "Tuvalu":
		c := CountryTuvalu
		return &c, nil
	case "U.S. Virgin Islands":
		c := CountryUSVirginIslands
		return &c, nil
	case "Uganda":
		c := CountryUganda
		return &c, nil
	case "Ukraine":
		c := CountryUkraine
		return &c, nil
	case "United Arab Emirates":
		c := CountryUnitedArabEmirates
		return &c, nil
	case "United Kingdom":
		c := CountryUnitedKingdom
		return &c, nil
	case "United States":
		c := CountryUnitedStates
		return &c, nil
	case "Uruguay":
		c := CountryUruguay
		return &c, nil
	case "Uzbekistan":
		c := CountryUzbekistan
		return &c, nil
	case "Vanuatu":
		c := CountryVanuatu
		return &c, nil
	case "Vatican":
		c := CountryVatican
		return &c, nil
	case "Venezuela":
		c := CountryVenezuela
		return &c, nil
	case "Vietnam":
		c := CountryVietnam
		return &c, nil
	case "Wallis and Futuna":
		c := CountryWallisAndFutuna
		return &c, nil
	case "Western Sahara":
		c := CountryWesternSahara
		return &c, nil
	case "Yemen":
		c := CountryYemen
		return &c, nil
	case "Zambia":
		c := CountryZambia
		return &c, nil
	case "Zimbabwe":
		c := CountryZimbabwe
		return &c, nil
	}
	return nil, errors.New("bad country")
}

type CountryPhoneCode string

const (
	CountryPhoneCodeGeorgia CountryPhoneCode = "Georgia +995"
)

type Language string

const (
	LanguageKa Language = "ka"
	LanguageEn Language = "en"
)

func (l Language) String() string {
	return string(l)
}

func LanguageFromString(lang string) (*Language, error) {
	switch lang {
	case "ka":
		l := LanguageKa
		return &l, nil
	case "en":
		l := LanguageEn
		return &l, nil
	default:
		return nil, errors.New("bad language")
	}
}

// Specialization is type for doctor specializations
type Specialization string

// this is the list of doctor specializations
const (
	SpecializationAllergology      Specialization = "allergology"
	SpecializationAngiology        Specialization = "angiology"
	SpecializationCardiology       Specialization = "cardiology"
	SpecializationDentistry        Specialization = "dentistry"
	SpecializationDermatology      Specialization = "dermatology"
	SpecializationDiabetology      Specialization = "diabetology"
	SpecializationEndocrinology    Specialization = "endocrinology"
	SpecializationGastroenterology Specialization = "gastroenterology"
	SpecializationGenetics         Specialization = "genetics"
	SpecializationGeriatrics       Specialization = "geriatrics"
	SpecializationGynecology       Specialization = "gynecology"
	SpecializationHematology       Specialization = "hematology"
	SpecializationHypertensiology  Specialization = "hypertensiology"
	SpecializationInfectiology     Specialization = "infectiology"
	SpecializationInternalMedicine Specialization = "internal_medicine"
	SpecializationLaryngology      Specialization = "laryngology"
	SpecializationNephrology       Specialization = "nephrology"
	SpecializationNeurology        Specialization = "neurology"
	SpecializationOncology         Specialization = "oncology"
	SpecializationOphthalmology    Specialization = "ophthalmology"
	SpecializationOrthopedics      Specialization = "orthopedics"
	SpecializationOther            Specialization = "other"
	SpecializationPsychiatry       Specialization = "psychiatry"
	SpecializationPulmonology      Specialization = "pulmonology"
	SpecializationRheumatology     Specialization = "rheumatology"
	SpecializationSurgery          Specialization = "surgery"
	SpecializationToxicology       Specialization = "toxicology"
	SpecializationTraumatology     Specialization = "traumatology"
	SpecializationUrology          Specialization = "urology"
	SpecializationVenereology      Specialization = "venereology"
)

func SpecializationFromString(str string) (*Specialization, error) {
	switch str {
	case "allergology":
		s := SpecializationAllergology
		return &s, nil
	case "angiology":
		s := SpecializationAngiology
		return &s, nil
	case "cardiology":
		s := SpecializationCardiology
		return &s, nil
	case "dentistry":
		s := SpecializationDentistry
		return &s, nil
	case "dermatology":
		s := SpecializationDermatology
		return &s, nil
	case "diabetology":
		s := SpecializationDiabetology
		return &s, nil
	case "endocrinology":
		s := SpecializationEndocrinology
		return &s, nil
	case "gastroenterology":
		s := SpecializationGastroenterology
		return &s, nil
	case "genetics":
		s := SpecializationGenetics
		return &s, nil
	case "geriatrics":
		s := SpecializationGeriatrics
		return &s, nil
	case "gynecology":
		s := SpecializationGynecology
		return &s, nil
	case "hematology":
		s := SpecializationHematology
		return &s, nil
	case "hypertensiology":
		s := SpecializationHypertensiology
		return &s, nil
	case "infectiology":
		s := SpecializationInfectiology
		return &s, nil
	case "internal_medicine":
		s := SpecializationInternalMedicine
		return &s, nil
	case "laryngology":
		s := SpecializationLaryngology
		return &s, nil
	case "nephrology":
		s := SpecializationNephrology
		return &s, nil
	case "neurology":
		s := SpecializationNeurology
		return &s, nil
	case "oncology":
		s := SpecializationOncology
		return &s, nil
	case "ophthalmology":
		s := SpecializationOphthalmology
		return &s, nil
	case "orthopedics":
		s := SpecializationOrthopedics
		return &s, nil
	case "other":
		s := SpecializationOther
		return &s, nil
	case "psychiatry":
		s := SpecializationPsychiatry
		return &s, nil
	case "pulmonology":
		s := SpecializationPulmonology
		return &s, nil
	case "rheumatology":
		s := SpecializationRheumatology
		return &s, nil
	case "surgery":
		s := SpecializationSurgery
		return &s, nil
	case "toxicology":
		s := SpecializationToxicology
		return &s, nil
	case "traumatology":
		s := SpecializationTraumatology
		return &s, nil
	case "urology":
		s := SpecializationUrology
		return &s, nil
	case "venereology":
		s := SpecializationVenereology
		return &s, nil
	}

	return nil, errors.New("bad specialization")
}

// Aggregate defines type for aggregate field in patient logs
type Aggregate string

//list of possible values for Aggregates
const (
	AggregateRoom             Aggregate = "Room"
	AggregateClinicInquiry    Aggregate = "ClinicInquiry"
	AggregateInterviewRoom    Aggregate = "InterviewRoom"
	AggregateCase             Aggregate = "Case"
	AggregateServiceRequest   Aggregate = "ServiceRequest"
	AggregateSchedule         Aggregate = "Schedule"
	AggregateInterview        Aggregate = "Interview"
	AggregateEvidence         Aggregate = "Evidence"
	AggregateLabtest          Aggregate = "Labtest"
	AggregateDiagnosis        Aggregate = "Diagnosis"
	AggregateQuestion         Aggregate = "Question"
	AggregateFile             Aggregate = "File"
	AggregateClinicConnection Aggregate = "ClinicConnection"
)

// ClinicAggregate defines type for aggregate field in clinic logs
type ClinicAggregate string

//list of possible values for Aggregates
const (
	ClinicAggregateDoctor        ClinicAggregate = "Doctor"
	ClinicAggregateDoctorService ClinicAggregate = "DoctorService"
	ClinicAggregateService       ClinicAggregate = "Service"
	ClinicAggregateOperator      ClinicAggregate = "Operator"
)

// ServiceCategory defines type for aggregate field in clinic logs
type ServiceCategory string

//list of possible values for Aggregates
const (
	ServiceCategoryDiagnostics  ServiceCategory = "Diagnostics"
	ServiceCategoryLaboratory   ServiceCategory = "Laboratory"
	ServiceCategoryConsultation ServiceCategory = "Consultation"
	ServiceCategoryProcedure    ServiceCategory = "Procedure"
)

func (sc ServiceCategory) String() string {
	return string(sc)
}

// CaseStatus defines stages for room with current case
type CaseStatus string

//list of possible values for Aggregates
const (
	CaseStatusDraft          CaseStatus = "draft"
	CaseStatusInterview      CaseStatus = "interview"
	CaseStatusAdditionalInfo CaseStatus = "additional_info"
	CaseStatusDiagnosis      CaseStatus = "diagnosis"
	CaseStatusComplete       CaseStatus = "complete"
)

func (sc CaseStatus) String() string {
	return string(sc)
}

func CaseStatusFromString(str string) *CaseStatus {
	switch str {
	case CaseStatusDraft.String():
		s := CaseStatusDraft
		return &s
	case CaseStatusInterview.String():
		s := CaseStatusInterview
		return &s
	case CaseStatusAdditionalInfo.String():
		s := CaseStatusAdditionalInfo
		return &s
	case CaseStatusDiagnosis.String():
		s := CaseStatusDiagnosis
		return &s
	case CaseStatusComplete.String():
		s := CaseStatusComplete
		return &s
	default:
		return nil
	}
}

type ChatSpaceType string

const (
	ChatSpaceTypeRoom          ChatSpaceType = "Room"
	ChatSpaceTypeInterviewRoom ChatSpaceType = "InterviewRoom"
	ChatSpaceTypeClinicInquiry ChatSpaceType = "ClinicInquiry"
)
