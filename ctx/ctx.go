package ctx

import (
	"context"
	"net/http"
	"path"
	"strings"

	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
	"github.com/nats-io/go-nats"
	"github.com/olivere/elastic"
)

type ctx struct {
	context context.Context
}

var c ctx

type Environment string

// Possible Env values
const (
	Production  Environment = "Production"
	Testing     Environment = "Testing"
	Development Environment = "Development"
)

// Key is a type for context keys
type key string

const (
	keyenv      key = "env"
	keypath     key = "path"
	keynats     key = "nats"
	keypostgres key = "postgres"
	keyelastic  key = "elastic"
	keyredis    key = "redis"
)

func Init() {
	c = ctx{context: context.Background()}
}

// func SetPath(value string) {
// 	c.context = context.WithValue(c.context, keypath, value)
// }

// func Path() string {
// 	return c.context.Value(keypath).(string)
// }

func SetEnv(value Environment) {
	c.context = context.WithValue(c.context, keyenv, value)
}

func Env() Environment {
	return c.context.Value(keyenv).(Environment)
}

func SetNats(value *nats.Conn) {
	c.context = context.WithValue(c.context, keynats, value)
}

func Nats() *nats.Conn {
	return c.context.Value(keynats).(*nats.Conn)
}

func SetRedis(value *redis.Client) {
	c.context = context.WithValue(c.context, keyredis, value)
}

func Redis() *redis.Client {
	return c.context.Value(keyredis).(*redis.Client)
}

func SetPG(value *sqlx.DB) {
	c.context = context.WithValue(c.context, keypostgres, value)
}

func PG() *sqlx.DB {
	return c.context.Value(keypostgres).(*sqlx.DB)
}

func SetES(value *elastic.Client) {
	c.context = context.WithValue(c.context, keyelastic, value)
}

func ES() *elastic.Client {
	return c.context.Value(keyelastic).(*elastic.Client)
}

// func ShiftPath() string {
// // 	p := Path()
// // 	p = path.Clean("/" + p)
// // 	i := strings.Index(p[1:], "/") + 1
// // 	if i <= 0 {
// // 		SetPath("/")
// // 		return p[1:]
// // 	}
// // 	SetPath(p[i:])
// // 	return p[1:i]
// // }

func SetPathToReqCtx(r *http.Request, path string) *http.Request {
	newCtx := context.WithValue(r.Context(), keypath, path)
	r = r.WithContext(newCtx)
	return r
}

func ShiftPathByReq(r *http.Request) (*http.Request, string) {
	p := r.Context().Value(keypath).(string)
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		r = SetPathToReqCtx(r, "/")
		return r, p[1:]
	}
	r = SetPathToReqCtx(r, p[i:])
	return r, p[1:i]
}
